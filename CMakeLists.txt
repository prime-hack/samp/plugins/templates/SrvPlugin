cmake_minimum_required(VERSION 3.5)

get_filename_component(PROJECT_NAME ${CMAKE_SOURCE_DIR} NAME)
project(${PROJECT_NAME} LANGUAGES CXX)
set(CMAKE_SHARED_LIBRARY_PREFIX "")

option(SUPPORT_AMX "Add support AMX events [ON/OFF]" %{SUPPORT_AMX})
option(SUPPORT_PROCESS_TICK "Add support process tick event [ON/OFF]" %{SUPPORT_PROCESS_TICK})

find_program(CCACHE_FOUND ccache)
if(CCACHE_FOUND)
	set_property(GLOBAL PROPERTY RULE_LAUNCH_COMPILE ccache)
	set_property(GLOBAL PROPERTY RULE_LAUNCH_LINK ccache)
	set(ENV{CCACHE_BASEDIR} "${CMAKE_SOURCE_DIR}")
endif(CCACHE_FOUND)

if (NOT CMAKE_CXX_COMPILER_VERSION VERSION_LESS 8.0)
	set(CMAKE_CXX17_STANDARD_COMPILE_OPTION "-std=c++17")
	set(CMAKE_CXX17_EXTENSION_COMPILE_OPTION "-std=gnu++17")
elseif (NOT CMAKE_CXX_COMPILER_VERSION VERSION_LESS 5.1)
	set(CMAKE_CXX17_STANDARD_COMPILE_OPTION "-std=c++1z")
	set(CMAKE_CXX17_EXTENSION_COMPILE_OPTION "-std=gnu++1z")
endif()

add_definitions("-ffunction-sections -fdata-sections")
if ("${CMAKE_BUILD_TYPE}" STREQUAL "MinSizeRel" OR "${CMAKE_BUILD_TYPE}" STREQUAL "Release")
	set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -Wl,--strip-all")
endif()


if(NOT IS_DIRECTORY "${CMAKE_SOURCE_DIR}/.git")
	execute_process(COMMAND git init WORKING_DIRECTORY ${CMAKE_SOURCE_DIR})
	execute_process(COMMAND git add CMakeLists.txt .clang-format .gitignore loader %{ProjectName}.h %{ProjectName}.cpp WORKING_DIRECTORY ${CMAKE_SOURCE_DIR})
endif()

include(FetchContent)
FetchContent_Declare(
  llmo
  GIT_REPOSITORY https://gitlab.com/SR_team/llmo
  GIT_TAG        no_SRSignal
)
FetchContent_Declare(
  sigslot
  GIT_REPOSITORY https://github.com/palacaze/sigslot
  GIT_TAG        19a6f0f5ea11fc121fe67f81fd5e491f2d7a4637 # v1.2.0
)
FetchContent_MakeAvailable(sigslot llmo)

file(GLOB ${PROJECT_NAME}_LIST
	${CMAKE_SOURCE_DIR}/*.c
	${CMAKE_SOURCE_DIR}/*.cpp
	${CMAKE_SOURCE_DIR}/*.cxx
	${CMAKE_SOURCE_DIR}/*.h
	${CMAKE_SOURCE_DIR}/*.hpp
	${CMAKE_SOURCE_DIR}/*.hxx
)

add_library(${PROJECT_NAME} SHARED loader/loader.cpp ${${PROJECT_NAME}_LIST})
target_link_libraries(${PROJECT_NAME} PRIVATE Pal::Sigslot llmo::old)

target_include_directories(${PROJECT_NAME} PRIVATE ${CMAKE_SOURCE_DIR})

target_compile_definitions(${PROJECT_NAME} PRIVATE PROJECT_NAME="${PROJECT_NAME}")
if(SUPPORT_AMX)
	target_compile_definitions(${PROJECT_NAME} PRIVATE SUPPORT_AMX)
endif()
if(SUPPORT_PROCESS_TICK)
	target_compile_definitions(${PROJECT_NAME} PRIVATE SUPPORT_PROCESS_TICK)
endif()

set_target_properties(${PROJECT_NAME} PROPERTIES
	CXX_STANDARD 17
	CXX_STANDARD_REQUIRED YES
	CXX_EXTENSIONS NO
	CXX_VISIBILITY_PRESET hidden
	C_VISIBILITY_PRESET hidden)

target_compile_options(${PROJECT_NAME} PUBLIC -m32)
target_link_options(${PROJECT_NAME} PUBLIC -m32)
target_link_options(${PROJECT_NAME} PRIVATE -static-libgcc -static-libstdc++)
