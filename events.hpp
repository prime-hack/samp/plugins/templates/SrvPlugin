#pragma once

#include <sigslot/signal.hpp>

struct PluginManager;
struct AMX;

namespace events{
	extern sigslot::signal<PluginManager &> InitPM;
#ifdef SUPPORT_AMX
	extern sigslot::signal<AMX &, int&> AmxLoad; /// args: context, result
	extern sigslot::signal<AMX &, int&> AmxUnload; /// args: context, result
#endif
#ifdef SUPPORT_PROCESS_TICK
	extern sigslot::signal<> ProcessTick;
#endif
}
