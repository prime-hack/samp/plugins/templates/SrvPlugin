#include "loader.h"

#include "../events.hpp"

namespace events {
	sigslot::signal<PluginManager &> InitPM;
#ifdef SUPPORT_AMX
	sigslot::signal<AMX &, int &> AmxLoad;
	sigslot::signal<AMX &, int &> AmxUnload;
#endif
#ifdef SUPPORT_PROCESS_TICK
	sigslot::signal<> ProcessTick;
#endif
} // namespace events

int Load( PluginManager *pluginManager ) {
	events::InitPM( *pluginManager );
	return 1;
}

void Unload() {}

#ifdef SUPPORT_AMX
int AmxLoad( AMX *amx ) {
	int result = 0;
	events::AmxLoad( *amx, result );
	return result;
}

int AmxUnload( AMX *amx ) {
	int result = 0;
	events::AmxUnload( *amx, result );
	return result;
}
#endif

#ifdef SUPPORT_PROCESS_TICK
void ProcessTick() {
	events::ProcessTick();
}
#endif

unsigned int Supports() {
	auto version = 0x200; // NOLINT
#ifdef SUPPORT_AMX
	version |= 0x10000; // NOLINT
#endif
#ifdef SUPPORT_PROCESS_TICK
	version |= 0x20000; // NOLINT
#endif
	return version;
}
