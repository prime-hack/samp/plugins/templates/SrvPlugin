#pragma once

struct PluginManager;
struct AMX;

extern "C" {
__attribute__((visibility("default"))) int Load( PluginManager *pluginManager );
__attribute__((visibility("default"))) void Unload();
#ifdef SUPPORT_AMX
__attribute__((visibility("default"))) int AmxLoad( AMX *amx );
__attribute__((visibility("default"))) int AmxUnload( AMX *amx );
#endif
#ifdef SUPPORT_PROCESS_TICK
__attribute__((visibility("default"))) void ProcessTick();
#endif
__attribute__((visibility("default"))) unsigned int Supports();
}
